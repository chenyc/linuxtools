# coding:utf8

__VERSION__ = "1.0.0@2022.05.11"
__DEBUG__ = False
__PROG__ = __file__
__DESCRIPTION__ = "linux typical command tools."

from prettytable import PrettyTable
from traceback import format_exc
import subprocess
import click


def print_version(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return
    click.echo(f"Version: {__VERSION__} ")
    ctx.exit()


@click.group(__PROG__)
@click.help_option("-h", "--help")
@click.option(
    "-v",
    "--version",
    is_flag=True,
    callback=print_version,
    expose_value=False,
    is_eager=True,
)
def cli(*args):
    pass


def print_help(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return
    click.echo(__VERSION__)
    ctx.exit()


def run_cmd(cmd, toSplit=False):
    stdout, stderr = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()
    result = stdout
    if isinstance(result, bytes):
        result = result.decode("utf-8")

    if toSplit:
        return result.split(), stderr
    else:
        return result, stderr


@click.help_option("-h", "--help")
@cli.command(name="port", help="Operate all port relation command")
@click.argument("pid", required=False, metavar="Pid", nargs=1)
@click.option("-i", "--info", is_flag=True, default=False, help="show all infomation")
@click.option("-a", "--all", is_flag=True, default=False, help="show all listen port infomation")
@click.option("-k", "--kill", is_flag=True, default=False, help="kill pid")
@click.pass_context
def port(ctx, pid, kill, info, all, *args):
    if True not in ctx.params.values():
        click.echo(ctx.get_help())
        ctx.exit()
    table = PrettyTable()

    table.field_names = ["COMMAND", "PID", "%CPU", "%MEM", "STAT", "TIME", "PORT"]
    ret, stderr = run_cmd(f"lsof -n -i -P | grep LISTEN ")
    pid_listen_port = dict()
    for process in ret.split("\n"):
        if not process:
            continue
        (
            _command,
            _pid,
            _user,
            _fd,
            _type,
            _device,
            _size,
            _node,
            _name,
            _,
        ) = process.split()
        ret, stderr = run_cmd(f" ps -o %cpu,%mem,stat,time  -p{_pid}|awk 'NR==2'", toSplit=True)
        if _pid not in pid_listen_port:
            pid_listen_port[_pid] = [
                _command,
                _pid,
                ret[0],
                ret[1],
                ret[2],
                ret[3],
                set(),
            ]
        pid_listen_port[_pid][-1].add(_name.split(":")[-1])
    if all:
        for key, value in pid_listen_port.items():
            value[-1] = "/".join(value[-1])
            table.add_row(value)
        print(table)

    if kill:
        if pid in pid_listen_port.keys():
            killProcess(pid)
        else:
            print(f"PID:{pid} not found")


def killProcess(pid):
    check = input(f"It will KILL UID:{pid}, Are Yor sure?   :")
    if check in ["y", "Y", "Yes", "yes", "YES"]:
        stdout, stderr = run_cmd(f"kill {pid}")
        if stderr == "":
            print("Kill SUCCESS")
    else:
        print("Cancel Kill")


if __name__ == "__main__":
    try:
        cli()
    except Exception as e:
        print(format_exc())
