Usage: lTools.py [OPTIONS] COMMAND [ARGS]...

Options:
  -h, --help     Show this message and exit.
  -v, --version

Commands:
  port  Operate all port relation command
